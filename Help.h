//
// Created by structix on 17.12.15.
//

#include <string>
#ifndef PWGEN_HELP_H
#define PWGEN_HELP_H


class Help {
public:
    std::string allcmds();
    std::string error();
    std::string leetspeak();
};


#endif //PWGEN_HELP_H

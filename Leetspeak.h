//
// Created by structix on 18.12.15.
//

#include <iostream>
#include <string>
#include <algorithm>
#ifndef PWGEN_LEETSPEAK_H
#define PWGEN_LEETSPEAK_H


class Leetspeak {
public:
    std::string convertString(std::string input);
private:
    void replaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace);
};


#endif //PWGEN_LEETSPEAK_H

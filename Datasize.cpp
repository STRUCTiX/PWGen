//
// Created by structix on 23.12.15.
//

#include "Datasize.h"


std::string Datasize::generatedSize(unsigned long input) {
    std::string suffix = "bytes", outputstr = "";
    float number = input;
    if (input >= 1000) {
        suffix = "Kilobytes";
        number = input / 1000;
    }
    if (input >= 1000000) {
        suffix = "Megabytes";
        number = input / 1000000;
    }
    if (input >= 1000000000) {
        suffix = "Gigabytes";
        number = input / 1000000000;
    }
    outputstr = std::to_string(std::lround(number)) + " " + suffix;
    return outputstr;
}

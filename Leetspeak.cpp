//
// Created by structix on 18.12.15.
//

#include "Leetspeak.h"

void Leetspeak::replaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace) {
    size_t pos = 0;
    while((pos = subject.find(search, pos)) != std::string::npos) {
        subject.replace(pos, search.length(), replace);
        pos += replace.length();
    }
}

std::string Leetspeak::convertString(std::string input) {
    std::transform(input.begin(), input.end(), input.begin(), ::tolower);
    replaceStringInPlace(input, "a", "4");
    replaceStringInPlace(input, "e", "3");
    replaceStringInPlace(input, "o", "0");
    replaceStringInPlace(input, "l", "1");
    replaceStringInPlace(input, "t", "7");
    replaceStringInPlace(input, "g", "6");
    replaceStringInPlace(input, "s", "5");
    replaceStringInPlace(input, "b", "8");
    return input;
}


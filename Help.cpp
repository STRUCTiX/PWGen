//
// Created by structix on 17.12.15.
//



#include "Help.h"

std::string Help::allcmds() {
    std::string outp = "Usage: PWGen [-c] <number of digits>\n-c: Use characters.\n-h: Display this text.\n";
    return outp;
}

std::string Help::error() {
    return "Error: Wrong syntax.\nType: PWGen -h for help\n";
}

std::string Help::leetspeak() {
    return "Usage: PWGen -1337 \"Your message\"\n";
}


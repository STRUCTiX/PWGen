//
// Created by structix on 17.12.15.
//

#include "Generator.h"


void Generator::startThreads(int mode, unsigned long anz) {
    if (mode == 1) {
        std::thread numberthread(&Generator::generator, this, anz);
        numberthread.join();
    }
    if (mode == 2) {
        std::thread ascii(&Generator::generator_ascii, this, anz);
        ascii.join();
    }
}

void Generator::generator(unsigned long anz) {
    std::string out = "";
    int counter = 0;
    int lastnum = 0;
    int newnum;
    while (counter < anz) {
        newnum = random(0, 9);
        if (newnum == lastnum) {
            while (newnum == lastnum) {
                newnum = random(0, 9);
            }
        }
        lastnum = newnum;
        out += std::to_string(newnum);
        counter++;
    }
    std::cerr << "Writing output.\n";
    std::cout << out << "\n";
}

void Generator::generator_ascii(unsigned long anza) {
    std::string out = "";
    int counter = 0;
    int rand1, rand1old = 0;
    int rand2, rand2old = 0;
    char character;
    while (counter < anza) {
        if (random(0,1) == 0) {
            rand1 = random(0, 9);
            if (rand1 == rand1old) {
                while (rand1 == rand1old) {
                    rand1 = random(0, 9);
                }
            }
            rand1old = rand1;
            out += std::to_string(rand1);
        } else {
            rand2 = random(33, 126);
            if (rand2 == rand2old) {
                while (rand2 == rand2old) {
                    rand2 = random(33, 126);
                }
            }
            rand2old = rand2;
            character = rand2;
            out += character;
        }
        counter++;
    }
    std::cerr << "Writing output.\n";
    std::cout << out << "\n";
}

int Generator::random(int lowerbounds, int upperbounds) {
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<int> uni(lowerbounds,upperbounds);
    auto random_integer = uni(rng);
    return random_integer;
}
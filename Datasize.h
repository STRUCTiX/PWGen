//
// Created by structix on 23.12.15.
//
#include <string>
#include <cmath>
#ifndef PWGEN_DATASIZE_H
#define PWGEN_DATASIZE_H


class Datasize {
public:
    std::string generatedSize(unsigned long input);
};


#endif //PWGEN_DATASIZE_H

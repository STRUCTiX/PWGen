//
// Created by structix on 17.12.15.
//
#include <iostream>
#include <random>
#include <string.h>
#include <thread>
#ifndef PWGEN_GENERATOR_H
#define PWGEN_GENERATOR_H


class Generator {
public:
    void startThreads(int mode, unsigned long anz);
private:
    int random(int lowerbounds, int upperbounds);
    void generator_ascii(unsigned long anza);
    void generator(unsigned long anz);
};


#endif //PWGEN_GENERATOR_H

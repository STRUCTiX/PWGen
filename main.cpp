#include <iostream>
#include "Help.h"
#include "Generator.h"
#include "Leetspeak.h"
#include "Datasize.h"


using namespace std;

int main(int argc, char *argv[]) {
    Generator g;
    Help h;
    Leetspeak ls;
    Datasize ds;
    if (argc == 2) {
        if (strcmp(argv[1], "-h") == 0) {
            cout << h.allcmds();
        } else if (strcmp(argv[1], "-1337") == 0) {
            cout << h.leetspeak();
        } else {
            cerr << ds.generatedSize(atoi(argv[1]));
            g.startThreads(1, atoi(argv[1]));
        }
    } else if (argc == 3 && strcmp(argv[1], "-c") == 0) {
        g.startThreads(4, 6);
        cerr << ds.generatedSize(atoi(argv[2])) + "\n";
        g.startThreads(2, atoi(argv[2]));
    } else if (strcmp(argv[1], "-1337") == 0 && strlen(argv[2]) > 0) {
        if (strcmp(argv[2], "-h") == 0) {
            cout << ls.convertString(h.allcmds());
        } else {
            cout << ds.generatedSize(strlen(argv[2])) << "\n";
            cout << ls.convertString(argv[2]) << "\n";
        }
    } else {
        cout << h.error();
    }
    cerr << "Success\n";
    return 0;
}
